﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CmdUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MultiparseTest()
        {

            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "drawto 10,10", myCanvas);
        }

        [TestMethod]
        public void ParameterSeperator()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "drawto 10,10", myCanvas);

        }

        [TestMethod]
        public void MoveToTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("moveto 10,10", "", myCanvas);

        }

        [TestMethod]
        public void RectangleTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("rectangle 10,10", "", myCanvas);
            cmdSetup.parse("square 10", "", myCanvas);
        }

        [TestMethod]
        public void CircleTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("circle 10", "", myCanvas);
        }

        [TestMethod]
        public void TriangleTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("drawto 100,80,20", "", myCanvas);
        }

        [TestMethod]
        public void ColorTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("pen black", "", myCanvas);
        }

        [TestMethod]
        public void FillTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("fill on", "", myCanvas);
        }

        [TestMethod]
        public void ClearTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("clear", "", myCanvas);
        }

        [TestMethod]
        public void ResetTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("reset", "", myCanvas);
        }

        [TestMethod]
        public void RunTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();

            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas myCanvas = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "", myCanvas);
        }

        [TestMethod]
        public void VarStoreTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();
            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;

            Graphical_Application.Canvas MyCanvass = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("value = 10", "", MyCanvass);
        }

        [TestMethod]
        public void VarAppendTest()
        {
            Graphical_Application.FormA form = new Graphical_Application.FormA();
            Graphical_Application.Parser cmdSetup = new Graphical_Application.Parser();
            Bitmap outBitmap = form.myBitmap;
            Graphical_Application.Canvas MyCanvass = new Graphical_Application.Canvas(Graphics.FromImage(outBitmap));
            cmdSetup.parse("value = value + 10", "", MyCanvass);
            cmdSetup.parse("value = value - 10", "", MyCanvass);
            cmdSetup.parse("value = value / 10", "", MyCanvass);
            cmdSetup.parse("value = value * 10", "", MyCanvass);
        }
    }
}
