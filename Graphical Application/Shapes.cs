﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Application
{
    /// <summary>
    /// Created abstract Shapes class to provide appropriate base classes from which other classes  inherit 
    /// </summary>
    abstract class Shapes
    {
        //Decleare variable to instantiate
        int height;
        int width;

        /// <summary>
        /// constructor to initialize the instance of class
        /// </summary>
        /// <param name="x">x assigns value from it child class</param>
        /// <param name="y">y assigns value from it child class</param>
        public Shapes(int x, int y)
        {
            height = x;
            width = y;
        }

        /// <summary>
        /// Draw method must be implemented by its child class and its is used to draw Shapes
        /// </summary>
        /// <param name="myCanvas">myCanvas hold drawing area results</param>
        public abstract void Draw(Canvas myCanvas);

    }
}
