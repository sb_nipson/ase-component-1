﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Graphical_Application
{
    /// <summary>
    /// This class check for Invalid parse and parameter Entered by User in single parse line
    /// and multiparse/program window.
    /// </summary>
    public class CheckUserSyntax
    {

        /// <summary>
        /// This method gives error message when entered parse will be Invalid
        /// </summary>
        /// <param name="myCanvas">This indicate a place where Error message will be Displayed</param>
        /// <param name="num">num indicate line number of parse</param>
        /// <param name="x">It indicate the location where error will be displayed on Canvas</param>
        public void parseCheck(Canvas myCanvas, int num, int x)
        {
            //create font for Error message
            Font errortxtFont = new Font("Arial", 10);
            //create Solid brush for Error message with color black
            SolidBrush errortxtBrush = new SolidBrush(Color.Black);
            num++;
            if (num != 0)
            {
                if (x == 0)
                {
                    //Reset Canvas if error not found
                    myCanvas.Reset();
                }
                //Display Error if parse on particular line does not exit
                myCanvas.g.DrawString("parse on line " + (num) + " does not exist", errortxtFont, errortxtBrush, 0, 0 + x);
            }
            else
            {
                //Display Error if parse does not exit
                myCanvas.g.DrawString("parse does not exist", errortxtFont, errortxtBrush, 0, 0);
            }
            //sets the error to true
            myCanvas.error = true;

        }


        /// <summary>
        /// This method gives error message when entered Parameter will be Invalid
        /// </summary>
        /// <param name="parameter">Gets boolean values according to validity of parameter</param>
        /// <param name="data">Gives line number where Error will be found</param>
        /// <param name="num">num indicate line number for parse</param>
        /// <param name="myCanvas">This indicate a place where Error message will be Displayed</param>
        /// <param name="x">It indicate the location where error will be displayed on Canvas</param>
        public void ParameterCheck(bool parameter, String data, int num, Canvas myCanvas, int x)
        {
            //Gives error message when entered Parameter will be Invalid
            if (!parameter)
            {
                Font errortxtFont = new Font("Arial", 10);
                SolidBrush errortxtBrush = new SolidBrush(Color.Black);
                if (x == 0)
                {
                    //Reset Canvas if error not found
                    myCanvas.Reset();
                }
                if ((num + 1) == 0)
                {
                    //Display Error if parameter are Invalid
                    myCanvas.g.DrawString("Paramater " + data + " is invalid", errortxtFont, errortxtBrush, 0, 0 + x);
                }
                else
                {
                    //Display Error if parameter are Invalid for Multi line parse
                    myCanvas.g.DrawString("Paramater " + data + " on line " + (num + 1) + " is invalid", errortxtFont, errortxtBrush, 0, 0 + x);
                }
                //Sets the Error to true
                myCanvas.error = true;
            }
        }

        /// <summary>
        /// This method gives error message when entered Parameter number will be Invalid and
        /// Catch Exception for invalid parse
        /// </summary>
        /// <param name="e">Catch Exception for invalid parse</param>
        /// <param name="num">num indicate line number of parse</param>
        /// <param name="myCanvas">This indicate a place where Error message will be Displayed</param>
        /// <param name="x">It indicate the location where error will be displayed on Canvas</param>
        public void ParameterCheck(Exception e, int num, Canvas myCanvas, int x)
        {
            Font errortxtFont = new Font("Arial", 10);
            SolidBrush errortxtBrush = new SolidBrush(Color.Black);

            if (x == 0)
            {
                //Reset Canvas if error not found
                myCanvas.Reset();
            }
            if ((num + 1) == 0)
            {
                //displays Error if number of parameters are invalid
                myCanvas.g.DrawString("Wrong number of parameters inputted", errortxtFont, errortxtBrush, 0, 0 + x);
            }
            else
            {
                //displays Error if number of parameters are invalid
                myCanvas.g.DrawString("Wrong number of parameters inputted on line" + (num + 1), errortxtFont, errortxtBrush, 0, 0 + x);
            }
            //sets the err to true
            myCanvas.error = true;
        }
    }
}
