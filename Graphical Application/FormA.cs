﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Application
{
    /// <summary>
    /// Created FormA class which is inherits from Form 
    /// This class hold method and properties for bitmap, slider, Menu bar and Enter event on command
    /// Prompt
    /// </summary>
    public partial class FormA : Form
    {
        //Instance data for slide show
        int pnl_SlideWidth;
        

        //Bitmap to draw which will display on pictureBox
        const int bitmapX = 640;
        const int bitmapY = 480;
        public Bitmap myBitmap = new Bitmap(bitmapX, bitmapY);
        public Graphics g;

        Canvas myCanvas;

        /// <summary>
        /// Created a FormA constructor to initialize
        /// </summary>
        public FormA()
        {
            InitializeComponent();
            pnl_SlideWidth = pnl_Slide.Width;
        
            myCanvas = new Canvas(Graphics.FromImage(myBitmap));
        }
       
        
        /// <summary>
        /// Action perform when Enter key will be pressed
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">Action perform when Enter key will be pressed</param>
        private void commandPrompt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ObtainInstruction(); //call ObtainInstruction Method
            }
        }

        /// <summary>
        /// Read Instruction provided by the user through Command Prompt, those instruction get converted 
        /// into Lower case and also Triming white spaces.
        /// readCommand to read single Line Instruction.
        /// readMultiCommand to read Multi Line Instruction.
        /// </summary>
        public void ObtainInstruction()
        {
            String readCommand = commandPrompt.Text.Trim().ToLower(); //fetches input of single line text field 
            String readMultiCommand = programWindow.Text.Trim().ToLower(); //fetches input of multi line text field 
            Parser cmdSetup = new Parser();  //creating obj of Parser
            cmdSetup.parse(readCommand, readMultiCommand, myCanvas); //calling a method of Parser and passing both textfield's input and alsoonject of canva
            Refresh();
        }

        /// <summary>
        /// Displays output depend on user input
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">passes an object specific to the event(output)</param>
        private void outputWindow_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            g.DrawImageUnscaled(myBitmap, 0, 0);
        }

        /// <summary>
        /// When Run Button Get clicked, Instruction inside command prompt get execute.
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">Pass an object for Run Event</param>
        private void Btn_Run_Click(object sender, EventArgs e)
        {
            ObtainInstruction();
        }

        /// <summary>
        /// Load the bitmap  
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">Pass an object for open Event</param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                outputWindow.Load(openFileDialog1.FileName);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// save the bitmap  
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">Pass an object for save Event</param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.ShowDialog();
                myBitmap.Save(saveFileDialog1.FileName);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Application get exit 
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">Pass an object for exit Event</param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit(); //Application get exit 
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        /// <summary>
        /// Provides information about application. 
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">Pass an object for about message box Event</param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Application: Graphical Application\n"

                + Environment.NewLine + "Developed by: Nipson K.C."
                , "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Provides information about Command use in this application. 
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">Pass an object for commandInformation message box Event</param>
        private void commandInformationToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("All COMMAND Should be Case INSENSITIVE"
              + Environment.NewLine + "======================================\n"
              + Environment.NewLine + "1. Circle \"To Draw Circle Shape\" Command-> circle radius\n"
              + Environment.NewLine + "2. Triangle \"To Draw Triangle Shape\" Command-> triangle hyp,base,adj\n"
              + Environment.NewLine + "3. rectangle \"To Draw Rectangle Shape\" Command-> rectangle width,Height\n"
              + Environment.NewLine + "4. moveTo \"To move the pen position\" Command-> moveTo x,y Where x and y is integer value or (X,y) coordinate\n"
              + Environment.NewLine + "5. drawTo \"To Draw Line on Drawing area\" Command-> drawTo x,y Where x and y is integer value or (X,y) coordinate\n"
              + Environment.NewLine + "6. run \"Read the program and Executes it\" Command-> run\n"
              + Environment.NewLine + "7. reset \"To move pen to initial position at the top left of the Screen\" Command-> reset\n"
              + Environment.NewLine + "8. loop for \"To Draw Shape using loop for\" Command-> loop for value //statement endfor\n"
              + Environment.NewLine + "9. while loop \"To Draw Shape using while loop\" Command-> if expression //statement endif\n"
              + Environment.NewLine + "10. if condition \"To Draw Shape using if condition\" Command-> while expression //statement endwhile\n"
              + Environment.NewLine + "11. while loop \"To Draw Shape using while loop\" Command-> if expression //statement endif\n"
              + Environment.NewLine + "12. Method\"To Draw Shape using method\" Command-> Method methodName(parameter) //statement endmethod\n"
              + Environment.NewLine + "13. Pen color \"To Choose color for drawing\" Command-> pen color where color indicate different colour name according to user choice\n"
              + Environment.NewLine + "14. Fill on \"To fill the interior of a polygon\" Command-> fill on"
              , "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Application get exit/Terminate when exit button will be pressed. 
        /// </summary>
        /// <param name="sender">provides a reference to the object that raised the event</param>
        /// <param name="e">Pass an object for exit button Event</param>
        private void Exit_btn_Click(object sender, EventArgs e)
        {
            Application.Exit();  // Terminate Application 
        }

        private void programWindow_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
