﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Application
{

    /// <summary>
    /// This class represent to store, retrive,delete and Check whether value is Exists in 
    /// Dictionary or not, with their specific keys and its stored Method.
    /// </summary>
    public class StoreMethod
    {
        //Dictionary represent to store key and its value
        Dictionary<string, string> VarDetail = new Dictionary<string, string>();

        /// <summary>
        /// Represent to add specific key and value of given method to the Dictionary
        /// </summary>
        /// <param name="methodName">store String Key of given method to the Dictionary</param>
        /// <param name="methodValue">store String Value of given method to the Dictionary</param>
        public void StoreVar(String methodName, String methodValue)
        {
            VarDetail.Add(methodName, methodValue); //add specific key and value of given method to the Dictionary
        }

        /// <summary>
        /// Represent to get value from specific key
        /// </summary>
        /// <param name="methodName">Get value of Specific key</param>
        /// <returns>Returns true, if Dictionary contains value with their specific Keys, otherwise false</returns>
        public String GetVar(String methodName)
        {
            String x;
            VarDetail.TryGetValue(methodName, out x); //get value from specific key
            return x;
        }

        /// <summary>
        /// boolean to Check for value is Exists or not
        /// </summary>
        /// <param name="methodName">value to check</param>
        /// <returns>Returns true, if Dictionary contains value with their specific Keys, otherwise false</returns>
        public bool VarExists(String methodName)
        {
            String x;
            return VarDetail.TryGetValue(methodName, out x);
        }

        /// <summary>
        /// Reset or Remove all keys and value from Dictionary
        /// </summary>
        public void Reset()
        {
            VarDetail.Clear(); //Remove all keys and value from Dictionary
        }
    }
}
